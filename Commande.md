## Développement d'une ressource *Commande*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *commande*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI                    | Action réalisée                               | Retour                                        |
|:----------|:-----------------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commandes             | prends toutes les commandes                   | 200 + tableau avec toutes les commandes       |
| GET       | /commandes/{id}        | prend la commande avec l'id                   | 200 + la commande récuperé avec l'id          |
|           |                        |                                               | 404 si l'id n'est pas connu                   |
| GET       | /commandes/{id}/pizzas | prend les pizzas présentes dans la commande   | 200 + un tableau avec les pizzas présentes dans la commande  |
|           |                        | d'identifiant id                              | 404 si l'id n'est pas connu                       |
| POST      | /commandes             | création d'une commande                       | 201 + l'URI de la ressource créée + représentation |
|           |                        |                                               | 400 si informations incorrectes |
|           |                        |                                               | 409 si le nom de la commande est déja pris    | 
| DELETE    | /commandes/{id}        | destruction de la commande d'identifiant id   | 204 si l'opération réalisée à fonctionnée                   |
|           |                        |                                               | 404 si l'id n'est pas connu                          |


Une commande comporte un identifiant et une liste de pizzas. Sa représentation JSON prendra donc la forme suivante :

{
	"id": 1,
	"pizzas": [1, 5]
}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni par la base de données.

{ "id": 1, "pizzas": [1, 5] }
