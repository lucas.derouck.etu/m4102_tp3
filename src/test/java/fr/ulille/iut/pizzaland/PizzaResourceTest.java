/*
package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester


public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
	private PizzaDao dao;
	private IngredientDao ingredientDao;

	@Override
	  protected Application configure() {
	   BDDFactory.setJdbiForTests();

	   return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
	    dao = BDDFactory.buildDao(PizzaDao.class);
	    ingredientDao = BDDFactory.buildDao(IngredientDao.class);
	    dao.createTable();
	}
	
	@After
	public void tearEnvDown() throws Exception {
	   dao.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();

		// Vérification de la valeur de retour (200)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// Vérification de la valeur retournée (liste vide)
		List<PizzaDto> ingredients;
		ingredients = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, ingredients.size());
	}
	
	@Test
	public void testGetExistingPizza() {

		Ingredient[] ingredients = { new Ingredient(1, "jambon") };
	    Pizza pizza = new Pizza();
	    pizza.setName("Reine");
	    pizza.setPrixpetite(10.5);
	    pizza.setPrixgrande(18.2);
		ArrayList<Long> list = new ArrayList<>();
		for (Ingredient ingredient : ingredients) {
			list.add(ingredient.getId());
		}
		pizza.setIngredients(list.toArray(new Ingredient[list.size()]));
	    long id = dao.insertNewPizza(pizza.getName(), pizza.getPrixpetite(), pizza.getPrixgrande(), list.toArray(list.size()));
	    pizza.setId(id);

	    Response response = target("/ingredients/" + id).request().get();

	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    Ingredient result = Ingredient.fromDto(response.readEntity(IngredientDto.class));
	    assertEquals(pizza, result);
	}
	
	@Test
	public void testGetNotExistingPizza() {
	  Response response = target("/pizzas/125").request().get();
	  assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	
	@Test
	public void testCreatePizza() {
	    IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
	    ingredientCreateDto.setName("Chorizo");

	    Response response = target("/ingredients")
	            .request()
	            .post(Entity.json(ingredientCreateDto));

	    // On vérifie le code de status à 201
	    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	    IngredientDto returnedEntity = response.readEntity(IngredientDto.class);

	    // On vérifie que le champ d'entête Location correspond à
	    // l'URI de la nouvelle entité
	    assertEquals(target("/ingredients/" +
			returnedEntity.getId()).getUri(), response.getLocation());
		
		// On vérifie que le nom correspond
	    assertEquals(returnedEntity.getName(), ingredientCreateDto.getName());
	}
		
	@Test
	public void testCreateSamePizza() {
	    IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
	    ingredientCreateDto.setName("Chorizo");
	    dao.insertPizza();

	    Response response = target("/ingredients")
	            .request()
	            .post(Entity.json(ingredientCreateDto));

	    assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateIngredientWithoutName() {
	    IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();

	    Response response = target("/ingredients")
	            .request()
	            .post(Entity.json(ingredientCreateDto));

	    assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingIngredient() {
	    Ingredient ingredient = new Ingredient();
	    ingredient.setName("Chorizo");
	    long id = dao.insert(ingredient.getName());
	    ingredient.setId(id);

	    Response response = target("/ingredients/" + id).request().delete();

	    assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	    Ingredient result = dao.findById(id);
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingIngredient() {
	    Response response = target("/ingredients/125").request().delete();
	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
		response.getStatus());
	}
	
	@Test
	public void testGetIngredientName() {
	    Ingredient ingredient = new Ingredient();
	    ingredient.setName("Chorizo");
	    long id = dao.insert(ingredient.getName());

	    Response response = target("ingredients/" + id + "/name").request().get();

	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    assertEquals("Chorizo", response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingIngredientName() {
	    Response response = target("ingredients/125/name").request().get();

	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
*/
