package fr.ulille.iut.pizzaland.dto;

import java.util.Arrays;

public class PizzaCreateDto {
	private String name;
	private double prixpetite;
	private double prixgrande;
	private Long[] ingredients;
	
	public PizzaCreateDto() {
		
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public double getPrixpetite() {
		return prixpetite;
	}

	public void setPrixpetite(double prixpetite) {
		this.prixpetite = prixpetite;
	}

	public double getPrixgrande() {
		return prixgrande;
	}

	public void setPrixgrande(double prixgrande) {
		this.prixgrande = prixgrande;
	}

	public Long[] getIngredients() {
		return ingredients;
	}

	public void setIngredients(Long[] ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "PizzaCreateDto [name=" + name + ", prixpetite=" + prixpetite + ", prixgrande=" + prixgrande
				+ ", ingredients=" + Arrays.toString(ingredients) + "]";
	}

	
	
	
}

