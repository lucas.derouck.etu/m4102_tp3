## Développement d'une ressource *Pizza*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *pizza*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI         | Action réalisée                               |retour                            |
|:----------|:------------|:----------------------------------------------|:---------------------------------
| GET       | /ingredients| prend tous les ingrédients          		  | 200 + tableau d'ingrédients  			|
| GET       | /ingredients/{id} | prend l'ingrédient avec l'id 			  | 200 + l'ingrédient                           |
|           |             |                                               | 404 si l'id n'est pas connu                        |
| GET       | /ingredients/{id}/name | prend le nom de l'ingrédient       | 200 + le nom de l'ingrédient                 |
|           |             | avec l'id                                     | 404 si l'id n'est pas connu                         |
| POST      | /ingredients | créé un ingrédient                           | 201 + l'URI de l'ingrédient créé + représentation |
|           |             |                                               | 400 si informations incorrectes |
|           |             |                                               | 409 si le nom de l'ingrédients est déja pris    |
| DELETE    | /ingredients/{id} | destruit l'ingrédients avec l'id 		  | 204 si l'opération réalisée à fonctionnée                   |
|           |             |                                               | 404 si l'id n'est pas connu                          |